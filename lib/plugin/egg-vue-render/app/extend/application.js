const fs = require('fs')
const path = require('path')
const axios = require('axios')
const { renderToString } = require('@vue/server-renderer')
const isProd = process.env.NODE_ENV === 'production'
const getConfig = () => {
  return JSON.parse(fs.readFileSync('.els/config.json').toString())
}
const getDevList = () => {
  return Object.keys(getConfig())
}
let proList = []
fs.readdir('./app/web/client', (err,data) => {
  if (data) {
    proList = data
  }
})

module.exports = {
  async vue(content) {
    const baseUrl = content.url.split('/')[1]
    const pro = getDevList().includes(baseUrl) || proList.includes(baseUrl) ? baseUrl : 'index'
    if (isProd || !getDevList().includes(pro)) {
      const html = fs.readFileSync(`app/web/client/${pro}/index.html`, 'utf-8')
      return html
    } else {
      if (getConfig()[pro] && getConfig()[pro].port) {
        const htmlRes = await axios.get(`http://localhost:${getConfig()[pro].port}`)
        return htmlRes.data
      } else {
        return `请先启动项目<${pro}>`
      }
    }
    
  },
  async vuessr(content) {
    const baseUrl = content.url.split('/')[1]
    const pro = getDevList().includes(baseUrl) || proList.includes(baseUrl) ? baseUrl : 'index'
    content.url = content.url.replace(`/${pro}`, '')
    if (isProd || !getDevList().includes(pro)) {
      const basePath = pro === 'index' ? `./app/web/server` : `./app/web/server/${pro}`
      const dirPath = path.resolve('./')
      const manifest = require(dirPath + `/app/web/server/${pro}/ssr-manifest.json`)
      const appPath = path.join(dirPath, basePath, manifest['app.js'])
      const createApp = require(appPath).default
      const { app, store } = await createApp(content)
      const indexTemplate = fs.readFileSync(
        path.join(dirPath, `/app/web/client/${pro}/index.html`),
        'utf-8'
      )
      const appContent = await renderToString(app)
      const html = indexTemplate
      .toString()
      .replace('<div id="app">', `<div id="app">${appContent}`)
      .replace('</body>', `</body><script>window.__INITIAL_STATE__=${JSON.stringify(store.state)}</script>`)
      return html
    } else {
      const basePath = pro === 'index' ? `./app/vue/server` : `./app/vue/server/${pro}`
      const dirPath = path.resolve('./')
      const manifest = require(dirPath + `/app/vue/server/${pro}/ssr-manifest.json`)
      const appPath = path.join(dirPath, basePath, manifest['app.js'])
      const createApp = require(appPath).default
      const htmlRes = await axios.get(`http://localhost:${getConfig()[pro].port}/index.html`)
      const indexTemplate = htmlRes.data
      const { app, store } = await createApp(content)
      
      const appContent = await renderToString(app)
      const html = indexTemplate
      .toString()
      .replace('<div id="app">', `<div id="app">${appContent}`)
      .replace('</body>', `</body><script>window.__INITIAL_STATE__=${JSON.stringify(store.state)}</script>`)
      return html
    }
  }
}