安装  
npm install
  
打包web  
npm run web:build  
  
开启服务端和前端项目  
npm run dev  
  
增加一个前端项目  
npm run dev:add 
  
渲染模式  
客户端渲染：await app.vue(content)  
服务端渲染：await app.vuessr(content)  
  
web的一个根目录文件代表一个前端项目，可以配置项目单独的打包配置  
client.config.js（ssr模式中的客户端执行文件）   
server.config.js（ssr模式中的服务端执行文件）  
vue.config.js（spa模式中的客户端执行文件）  
