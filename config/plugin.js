const path = require('path');
exports.vueRender = {
  enable: true,
  path: path.join(__dirname, '../lib/plugin/egg-vue-render'),
};