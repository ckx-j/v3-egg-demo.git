const path = require('path')
module.exports = appInfo => {
  const config = exports = {};

  config.keys = appInfo.name + '_1636985709507_3177';

  config.middleware = [];

  config.static = {
    prefix: '/', 
    dir: [path.join(appInfo.baseDir, 'app/web/client'), path.join(appInfo.baseDir, 'app/vue/server')],
    dynamic: true,
    preload: false, 
    maxAge: 0,
    buffer: false,
  }

  const userConfig = {
  };

  return {
    ...config,
    ...userConfig,
  };
};