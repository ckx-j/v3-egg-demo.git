import { createApp } from 'vue'
import { createWebHistory } from 'vue-router'
import createRouter from './router'
import createStore from './store'
import App from './App.vue'

import '@/assets/svg/svg.js'
import Svg from '@/components/Svg.vue'

const app = createApp(App)
const router = createRouter(createWebHistory('user'))
const store = createStore()
app.use(router)
app.use(store)

app.component('Svg', Svg)

app.mount('#app')