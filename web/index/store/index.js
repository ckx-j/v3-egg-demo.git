import { createStore } from 'vuex'

// 创建一个新的 store 实例
const store = createStore({
  state () {
    return {
      count: 0
    }
  },
  mutations: {
    initSsrState (state, pload) {
      pload.forEach(item => {
        for (let i in item) {
          state[i] = item[i]
        }
      })
      
    }
  }
})

export default function () {
  return store
}