import { createRouter } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../views/home.vue'),
    children: [
      {
        path: '/new1',
        name: 'new1',
        component: () => import('../views/new.vue')
      },
      {
        path: '/about',
        name: 'about',
        component: () => import('../views/about.vue')
      }
    ]
  },
  {
    path: '/new',
    name: 'new',
    component: () => import('../views/new.vue')
  }
]

export default function (history) {
  return createRouter({
    history,
    routes
  })
}