'use strict';

const Controller = require('egg').Controller;

class HomeController extends Controller {
  async index() {
    const { ctx, app } = this
    const content = {
      url: ctx.originalUrl
    }
    ctx.body = await app.vue(content)
  }
  async ssr() {
    const { ctx, app } = this;
    const content = {
      url: ctx.originalUrl
    }
    // if (ctx.originalUrl === '/user') {
    //   ctx.redirect(`http://127.0.0.1:7001/user/`);
    // }
    ctx.body = await app.vuessr(content)
  }
  async api() {
    const { ctx, app } = this;
    const content = {
      url: ctx.originalUrl,
      text: '请求地址是：' + ctx.originalUrl
    }
    ctx.body = content
  }
}

module.exports = HomeController;