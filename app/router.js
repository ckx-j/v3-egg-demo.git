module.exports = app => {
  const { router, controller } = app;
  router.get('/api', controller.home.api);

  router.get('/user', controller.home.index);
  router.get('/user/*', controller.home.index);

  router.get('/*', controller.home.ssr);
};